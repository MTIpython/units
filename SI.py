r"""
.. module:: MTIpython.units.SI
    :platform: Unix, Windows
    :synopsis: Unit registry and helper functions

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>

.. py:data:: u

   The default :class:`~pint:pint.UnitRegistry` handling all unit safe calculations.

   .. warning::

      RPM (revolutions per minute) is defined as default by:

      .. math::
         rpm = \frac{1}{[min]}

      and **not** as:

      .. math::
         rpm_rad = \frac{2 \pi [rad]}{[min]}

      See `issue #57 <http://mti-gitlab.ihc.eu/generic-software/MTIpython/issues/57>`_ on Gitlab, for a
      discussion regarding this behaviour.

      To change this behaviour see :func:`.set_rpm_definition`

.. py:data:: _Q

   A generic :class:`~pint:.Quantity` allows for the creation of custom Quatities.

"""
import warnings
import pint
from collections.abc import Iterable
from pkg_resources import resource_filename

std_path = resource_filename(__name__, '../resources/units/MTIpython_units.txt')

__all__ = ['u', '_Q', 'isquantity', 'isunit', 'arequantities', 'isiterable', 'set_rpm_definition',
           'dimensionalty_docstring']

u = pint.UnitRegistry(autoconvert_offset_to_baseunit=True)
u.load_definitions(std_path)
r"""
The pint InitRegistry, all quantities and units needs to be registered at the same UnitRegistry
"""

u.default_format = '~.3'

_Q = u.Quantity
r"""
The Quantity registered with the UnitRegistry, which can be used to create custom Quantities
"""

pint.set_application_registry(u)

warnings.simplefilter('ignore', category=pint.UnitStrippedWarning)

def isquantity(obj):
    r"""
    Check if object is a :class:`~pint:.Quantity`

    Args:
        obj: object to check

    Returns:
        bool: True if the object is a :class:`~pint:.Quantity`
    """
    return isinstance(obj, u.Quantity)


def isunit(obj):
    r"""
    Check if object is a :class:`~pint:.Unit`

    Args:
        obj (obj): object to check

    Returns:
        bool: True if object is a :class:`~pint:.Unit`
    """
    return isinstance(obj, u.Unit)


def arequantities(*args):
    r"""
    Check if all objects are :class:`~pint:.Quantity`

    Args:
        *args (obj): objects to check

    Returns:
        bool: True if all objects are :class:`~pint:.Quantity`
    """
    return all([isquantity(a) for a in args])


def isiterable(obj):
    r"""
    Check if object is iterable. This check is :class:`~pint:.Quantity` safe

    Args:
        obj (obj): object to check

    Returns:
        bool: True if object is iterable
    """
    if isquantity(obj):
        return isinstance(obj.m, Iterable)
    else:
        return isinstance(obj, Iterable)


def set_rpm_definition(with_rad=False):
    r"""
    Change the definition of :math:`[rpm]`, which is by default defined as

    .. math::
       rpm = \frac{1}{[min]}

    and **not** as:

    .. math::
       rpm = \frac{2 \pi [rad]}{[min]}

    When the :math:`[rad]` behaviour is desired, you'll have to manually set it each time after you have imported
    MTIpython. See `issue #57 <http://mti-gitlab.ihc.eu/generic-software/MTIpython/issues/57>`_ on Gitlab, for a
    discussion regarding this behaviour.

    Args:
        with_rad (bool): When set to True :math:`rpm = \frac{2 \pi [rad]}{[min]}` is used, otherwise the MTIpython
         default behaviour is set. False will set the behaviour back to default.
    """
    if with_rad:
        u.revolutions_per_minute = u.rpm_rad
        u.rpm = u.revolutions_per_minute
    else:
        u.load_definitions(std_path)


def dimensionalty_docstring(unit):
    r"""
    Get the dimensionality docstring

    Example usage:

        >>> dimensionalty_docstring(u.N * u.m)
        ':math:`[M^{1} L^{2} t^{-2}]`'

    Args:
        unit (:class:`~pint.Unit`): the unit to convert

    Returns:
        str: A string representing the dimensionalty of the unit
    """
    return ':math:`[{}]`'.format(' '.join(
        ['{dim:}^{{{pow:}}}'.format(dim=d[1:2].capitalize() if d != '[time]' else 't', pow=int(p)) for d, p in
         unit.dimensionality.items()]))
